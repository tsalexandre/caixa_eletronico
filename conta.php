<?php

   header('Content-Type: text/html; charset=utf-8');

   class Conta{
      protected $n_conta = "123.456.789.0";
      protected $senha   = "123456";
      public    $saldo;
   }

   class Movimentacao extends Conta{
      // Função Sacar
      public function setSacar(){
         $numeroNotas = array(100, 50, 20, 10);
         $saque = $_GET['valor'];

         //Pega o o ultimo numero
         $ult_numero = substr($saque, -1);

         //verifica entradas nao existentes
         if($saque > 0 && $ult_numero <= 9 && $ult_numero >= 1){
            $this->valor = array("error" => "BillUnavaiableException");
         }elseif($saque == null OR empty($saque)){
            $this->valor = array("0" => 0);
         }elseif($saque < 0 OR !is_numeric($saque)){
            $this->valor = array("error" => "InvalidArgumentException");
         }elseif($saque > 0){
            $a = count($numeroNotas);
            //imprime notas
            for($i=0; $i<$a; $i++){
               $result = $saque / $numeroNotas[$i];
               if($result >= 1){
                  $saque = $saque % $numeroNotas[$i];
                  $this->valor[] = [$numeroNotas[$i]=>intval($result)];
               }
            }
         }else{
         }
      }
      public function getSacar(){
         return json_encode($this->valor);
      }
   }

   //Instanciando
   $acao = new Conta;
   $action = new Movimentacao;

   //Sacando
   $action->setSacar();
   echo $action->getSacar();
