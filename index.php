<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <title>Document</title>
   <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
   <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
   <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
   <script type="text/javascript">
      $("document").ready(function(){
         $(".formulario").submit(function(){
            var dados = $(this).serialize();
            $(".retorno").html("Saque:" + dados);
            $.ajax({
               type: "GET",
               dataType: "html",
               url: "conta.php",
               data: dados,
               success: function(data){
                  $(".retorno").html(data);
               }
            });
            return false;
         });
      });
   </script>
</head>
<body>
   <style type="text/css">
      .center{
         margin: 50px auto;
         width: 400px;
      }
      body{
         background: #ccc;
      }
   </style>
   <div class="container">
      <div class="center">
         <form class="formulario" method="post">
            <h1>Caixa Eletronico</h1>
            <div class="form-group">
               <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-usd" aria-hidden="true"></i></span>
                  <input type="text"class="form-control" placeholder="Insira o valor" name="valor" id="valor">
               </div>
            </div>
            <input type="submit" class="btn btn-primary" value="Enviar">
         </form>
         <div class="retorno"></div>
      </div>
   </div>
</body>
</html>
